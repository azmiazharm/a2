//var let const
var a = 123;
var a = "diganti";

let b = 213; // let ga bisa di re-declare
// let b = '123'; 

const c = 1; // const ga bias di ganti nilainya setelah dibuat
// c = 32

// string, number, bool, null, undefined
const nama = 'azmi';
const umur = 22;
const happines = 0.7;
const isAlive = true;
const kosong = null;
const undef = undefined;
const halo = 'Halo dunia'

// concat
console.log('Halo saya ' + nama + ', umur saya ' + umur + ' tahun');
//string literal
console.log(`Halo saya ${nama}, umur saya ${umur} tahun`);

console.log(nama.toUpperCase())
console.log(nama.substring(2,4))
console.log(halo.split(' '))

// Array
const buah = ['apel', 'jeruk', 'nanas'];
buah[3] = 'anggur'
console.log(buah)

buah.push('pear') //tambah dibelakang
buah.unshift('durian') //tambah didepan

//Object

const person = {
    firstName: nama,
    lastName: 'azhar',
    age: umur,
    hobby: ['gaming'],
    address: {
        street: 'Kebon Jeruk',
        city: 'Jakarta',
    }
}

console.log(person.firstName)
console.log(person.hobby[0])

// array of obj
const todos = [
    {
        id: 1,
        text: 'learn js',
        isCompleted: true
    },
    {
        id: 2,
        text: 'learn devtools',
        isCompleted: true
    },
    {
        id: 3,
        text: 'learn golang',
        isCompleted: false
    },
];

// for(let i = 0; i < todos.length; i++){
//     console.log(`index ke ${i}: ${todos[i].text}`)
// }

// for(let todo of todos){
//     console.log(todo.text)
// }

// todos.forEach(function(todo) {
//     console.log(todo.text)
// })

// const todoText = todos.map(function(todo) {
//     return todo.text;
// })

// console.log(todoText);

const todoText = todos.filter(function(todo) {
    return todo.isCompleted === false;
}).map(function(todo) {
    return todo.text
})

console.log(todoText);

// if-else
const jml = 10;
const color = jml >10 ? 'red' : 'blue'

function sumTwoNum(num1 = 1, num2 = 2) {
    return num1 + num2;
}

// console.log(sumTwoNum(2))

// Constructor
function Person(firstName, lastName, dob){
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);

    this.getBirthYear = function() {
        return this.dob.getFullYear();
    }
}

// fungsi di dalem sama kaya prototype, bedanya 
// prototype ga keliatan di constructor
Person.prototype.getFullName = function() {
    return `${this.firstName} ${this.lastName}`;
}

//Instantiate
const orang1 = new Person('Azmi', 'Azhar', '4-1-2000')
const orang2 = new Person('Azhar', 'Musyaffa', '8-1-2040')

console.log(orang1.getFullName());

// lebih mudah pakai class
class Personz{
    constructor(firstName, lastName, dob){
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);

        this.getBirthYear = function() {
            return this.dob.getFullYear();
        }
    }
}