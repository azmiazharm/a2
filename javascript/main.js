const myForm = document.querySelector('#my-form')
const nameInput = document.querySelector('#name')
const emailInput = document.querySelector('#email')
const msg = document.querySelector('.msg')
const userList = document.querySelector('#users')

myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
    e.preventDefault();

    if(nameInput.value === '' || emailInput === '') {
        msg.classList.add('error');
        msg.innerHTML = 'Please enter all fields';

        setTimeout(() => {
            msg.remove()
        }, 3000);
    } else {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(
            `${nameInput.value} : ${emailInput.value}`
        ));

        userList.appendChild(li);
    }
}


// // get single element
// console.log(document.getElementById('my-form'))
// console.log(document.querySelector('.container'))

// // get multi element
// console.log(document.querySelectorAll('.item'));
// console.log(document.getElementsByClassName('item')) // jadinya html collection, bukan array
// console.log(document.getElementsByTagName('li')) // jadinya html collection, bukan array


// const ul = document.querySelector('.items');

// ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'diganti dari js'
// ul.children[1].innerText= '<h1>Innertext</h1>';
// ul.lastElementChild.innerHTML = '<h1>Innertext</h1>';

// const btn = document.querySelector('.btn');

// btn.addEventListener('click', (e) => {
//     e.preventDefault();
//     document.querySelector('#my-form').style.background = 
//     '#ccc';

//     document.querySelector('body').classList.add('bg-dark');
//     document.querySelector('.items').lastElementChild.innerHTML = '<h1>Helloo</h1>'

//     // console.log(e.target);
//     // console.log('click');
// })